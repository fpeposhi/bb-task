
    <div class="row mt-2">
        <div class="col-md-12">
            <div class="page-header">
                <h3>Edit Order
                    <a href="/orders" class="btn btn-info float-right">Back to list</a>
                </h3>
            </div>

        </div>
    </div>

    <div class="row mt-2 mb-5">
        <div class="col-md-12">
            {% for  message in flashSession.getMessages('error') %}
                <div class="alert alert-danger">
                    {{ message }}
                </div>
            {% endfor %}

            <form action="/orders/save" class="form" method="post">

                <input type="hidden" name="id" value="{{ order.id }}">
                <input type='hidden'
                       name='{{ this.security.getTokenKey()  }}'
                       value='{{ this.security.getToken()  }}'/>

                <input type="hidden" name="id" value="{{ order.id }}">

                <div class="row">
                    <div class="form-group col-md-2">
                        <label for="name">Order No <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="order_no" value="{{ order.order_no }}" >
                    </div>

                    <div class="form-group col-md-3">
                        <label for="name">Order Date <span class="text-danger">*</span></label>
                        <input type="date" class="form-control" name="order_date"  value="{{ order.order_date }}"  >
                    </div>
                    <div class="form-group col-md-3">
                        <label for="name">User <span class="text-danger">*</span></label>
                        <select class="form-control" name="id_user"  >
                            {% for user in users %}
                                <option {% if user.id == order.user.id %} selected {% endif %} value="{{ user.id }}">{{ user.name }} {{ user.surname }}</option>
                            {% endfor %}
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-8">
                        <label for="name">Order notes</label>
                        <input type="text" class="form-control" name="order_notes" value="{{ order.order_notes }}" >
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-8">
                        <h6>Order Items</h6>

                        <table class="table table-order-items">
                            <thead>
                            <tr>
                                <th>Item <span class="text-danger">*</span></th>
                                <th width="156px">Quantity <span class="text-danger">*</span></th>
                                <th>Price</th>
                                <th class="">
                                    <button class="btn btn-success btn-sm float-right btn-add-order-item" type="button">Add</button>
                                </th>
                            </tr>
                            </thead>

                            <tbody>
                            {% for orderItem in order.orderItems %}
                                <tr>
                                    <td>
                                        <select class="form-control item" name="item_id[]"  >
                                            <option value="">-- select an item -- </option>
                                            {% for item in items %}
                                                <option {% if item.id == orderItem.item.id %} selected {% endif %}  data-price="{{ item.item_price }}" value="{{ item.id }}">{{ item.item_name }}</option>
                                            {% endfor %}
                                        </select>
                                    </td>
                                    <td>
                                        <input type="number" class="form-control item-quantity" name="quantity[]" value="{{ orderItem.quantity }}" >
                                    </td>
                                    <td>
                                        <span class="price">
                                            {{ orderItem.item.item_price }}
                                        </span>

                                    </td>
                                    <td>
                                        <button class="btn btn-danger btn-sm float-right btn-remove-item" type="button">Remove</button>
                                    </td>
                                </tr>
                            {% endfor %}
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="2" class=""><span class="float-right">Total:</span></td>
                                <td colspan="1" class=""><span class="total float-right"></span></td>
                                <td></td>
                            </tr>
                            </tfoot>
                        </table>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 ">
                        <hr>
                        <button class="btn btn-danger float-left btn-delete-order" data-id="{{ order.id }}" type="button">Delete this order</button>
                        <a class=" float-right btn btn-link" href="/users">Cancel</a>
                        <button class="btn btn-success float-right" type="submit">Save Details</button>
                    </div>
                </div>

            </form>
        </div>
    </div>



