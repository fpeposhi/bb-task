<div class="row mt-2">
    <div class="col-md-12">
        <div class="page-header">
            <h3>Orders of {{ user.name }} {{ user.surname }}
                <a href="/users/" class="btn btn-info float-right">Back to Users</a>
            </h3>
        </div>

    </div>
</div>


<div class="row col-md-12 mt-2">

    <table class="table table-stripped">
        <thead>
        <tr>
            <th>Order No</th>
            <th>User</th>
            <th>Delivered</th>
            <th>Order Date</th>
            <th>Order Amount</th>
            <th></th>
        </tr>
        </thead>

        <tbody>
        {% for order in user.orders %}
            <tr>
                <td>{{ order.order_no }}</td>
                <td>{{ order.user.name }} {{ order.user.surname }}</td>
                <td>{{ order.is_delivered ? 'Yes' : 'No' }}</td>
                <td>{{ order.order_date }}</td>
                <td>{{ order.order_amount }}</td>
                <td><a href="/orders/edit/{{ order.id }}" class="">Edit</a></td>
            </tr>
        {% endfor %}

        </tbody>

    </table>

</div>
