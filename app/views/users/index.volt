<div class="row mt-2">
    <div class="col-md-12">
        <div class="page-header">
            <h3>Users
                <a href="/users/create" class="btn btn-info float-right">Add new user</a>
            </h3>
        </div>

    </div>
</div>


<div class="row col-md-12 mt-2">

    {% for  message in flashSession.getMessages('warning') %}
        <div class="alert alert-warning">
            {{ message }}
        </div>
    {% endfor %}

    {% for message in flashSession.getMessages('error') %}
        <div class="alert alert-danger">
            {{ message }}
        </div>
    {% endfor %}

    <table class="table table-stripped">
        <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th></th>
            <th></th>
        </tr>
        </thead>

        <tbody>
        {% for user in users %}
            <tr>
                <td>{{ user.name }} {{ user.surname }}</td>
                <td>{{ user.email }}</td>
                <td>
                    <a href="/users/edit/{{ user.id }}" class="">Edit</a>
                </td>
                <td>
                    <a href="/orders/user/{{ user.id }}" class="">Orders</a>
                </td>
            </tr>
        {% endfor %}

        </tbody>

    </table>

</div>
