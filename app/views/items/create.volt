
    <div class="row mt-2 item-container">
        <div class="col-md-12">
            <div class="page-header">
                {% if item %}
                    <h3>Edit Item {{ item.item_name }}
                        <a href="/items" class="btn btn-info float-right">Back to list</a>
                    </h3>
                {%  else %}
                    <h3>Add Item
                        <a href="/items" class="btn btn-info float-right">Back to list</a>
                    </h3>
                {%  endif %}
            </div>

        </div>
    </div>

    <div class="row mt-2">
        <div class="col-md-12">
            {% for  message in flashSession.getMessages('error') %}
                <div class="alert alert-danger">
                    {{ message }}
                </div>
            {% endfor %}

            <form action="/items/save" class="form" method="post">

                {% if item %}
                    <input type="hidden" name="id" value="{{ item.id }}">
                {%  endif %}

                <input type='hidden'
                       name='{{ this.security.getTokenKey()  }}'
                       value='{{ this.security.getToken()  }}'/>

                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="name">Name  <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" {% if item  %} value="{{ item.item_name }}" {% endif %} name="item_name" >
                    </div>

                    <div class="form-group col-md-3">
                        <label for="price">Price  <span class="text-danger">*</span></label>
                        <input type="number" class="form-control" {% if item  %} value="{{ item.item_price }}" {% endif %}  name="item_price"  >
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-7 ">
                        <hr>
                        {% if item %}
                        <button class="btn btn-danger float-left btn-delete-item" data-id="{{ item.id }}" type="button">Delete Item</button>
                        {% endif %}
                        <a class=" float-right btn btn-link" href="/items">Cancel</a>
                        <button class="btn btn-success float-right" type="submit">Save Details</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
