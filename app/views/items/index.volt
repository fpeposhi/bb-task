<div class="row mt-2">
    <div class="col-md-12">
        <div class="page-header">
            <h3>Items
                <a href="/items/create" class="btn btn-info float-right">Add new Item</a>
            </h3>
        </div>

    </div>
</div>


<div class="row col-md-12 mt-2">


    {% for  message in flashSession.getMessages('warning') %}
        <div class="alert alert-warning">
            {{ message }}
        </div>
    {% endfor %}

    {% for message in flashSession.getMessages('error') %}
        <div class="alert alert-danger">
            {{ message }}
        </div>
    {% endfor %}

    <table class="table table-stripped">
        <thead>
        <tr>
            <th>Item</th>
            <th>Price</th>
            <th></th>
        </tr>
        </thead>

        <tbody>
        {% for item in items %}
            <tr>
                <td>{{ item.item_name }}</td>
                <td>{{ item.item_price }}</td>
                <td><a href="/items/create/{{ item.id }}" class="">Edit</a></td>
            </tr>
        {% endfor %}

        </tbody>

    </table>

</div>
