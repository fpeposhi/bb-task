create table "user"
(
    id         SERIAL PRIMARY KEY,
    name       varchar(100) not null,
    surname    varchar(100) not null,
    email      varchar(100) unique,
    is_active  smallint  default 1,

    created_at timestamp default CURRENT_TIMESTAMP,
    updated_at timestamp DEFAULT CURRENT_TIMESTAMP
);
create table "item"
(
    id         SERIAL PRIMARY KEY,
    item_name  varchar(255) not null,
    item_price numeric   default 0,

    created_at timestamp default CURRENT_TIMESTAMP,
    updated_at timestamp DEFAULT CURRENT_TIMESTAMP
);
create table "order"
(
    id           SERIAL PRIMARY KEY,
    id_user      integer REFERENCES "user" (id),
    order_no     varchar(30) not null,
    order_date   date,
    order_amount numeric      default 0,
    order_notes  varchar(255) DEFAULT null,
    is_delivered SMALLINT     default 0,

    created_at   timestamp    default current_timestamp,
    updated_at   timestamp    DEFAULT CURRENT_TIMESTAMP
);
create table "order_items"
(
    id         SERIAL PRIMARY KEY,
    id_order   integer REFERENCES "order" (id),
    id_item    integer REFERENCES "item" (id),
    quantity   numeric   default 0,

    created_at timestamp default CURRENT_TIMESTAMP,
    updated_at timestamp DEFAULT CURRENT_TIMESTAMP
);