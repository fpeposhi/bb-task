<?php
namespace App\Validator;

use Phalcon\Mvc\Controller;
use Phalcon\Validation;

abstract class BaseValidator
{
    protected $validation = null;

    public function __construct()
    {
        $this->validation = new Validation();
    }

    public abstract function validate($data);

    public function getMessages(){
        return $this->validation->getMessages();
    }

    public function isValid(){
        return count($this->getMessages()) == 0;
    }

    public function backWithErrors(Controller $controller){
        $controller->view->disable();

        $messages = $this->getMessages();
        foreach ($messages as $message) {
            $controller->flashSession->error($message->getMessage());
        }
        $controller->response->redirect($controller->request->getHTTPReferer(), true);
        return true;
    }

}