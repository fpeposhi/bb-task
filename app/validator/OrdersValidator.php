<?php
namespace App\Validator;

use Phalcon\Validation\Validator\Date;
use Phalcon\Validation\Validator\PresenceOf;

class OrdersValidator extends BaseValidator
{

    public function validate($data)
    {
        $this->validation->add([
            "id_user", "order_no"
        ],
            new PresenceOf([
                'message' => [
                    "id_user" => 'User is required',
                    "order_no" => 'Order No is required',
                ]
            ])
        );

        $this->validation->add(
            "item_id",
            new PresenceOf([
                'message' => 'You must select at least one item for this order'
            ])
        );

        $this->validation->add(
            "order_date",
            new Date([
                "format" => [
                    "order_date" => "Y-m-d",
                ],
                "message" => [
                    "order_date" => "The date is invalid. Required format: Year-month-day",
                ],
            ])
        );
        $this->validation->validate($data);

        return $this;
    }


}