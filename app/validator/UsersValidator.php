<?php
namespace App\Validator;

use Phalcon\Validation\Validator\PresenceOf;

class UsersValidator extends BaseValidator
{

    public function validate($data)
    {
        $this->validation->add(
            ['name', "surname"],
            new PresenceOf(
                [
                    'message' => [
                        'name' => 'Name is required',
                        'surname' => 'Surname is required'
                    ],
                ]
            )
        );

        $this->validation->validate($data);

        return $this;
    }


}