<?php
namespace App\Validator;

use Phalcon\Validation\Validator\Numericality;
use Phalcon\Validation\Validator\PresenceOf;
use PositiveValue;

class ItemsValidator extends BaseValidator
{

    public function validate($data)
    {
        $this->validation->add([
            "item_name", "item_price"
        ], new PresenceOf([
            'message' => [
                'item_name' => 'Item name is required',
                'item_price' => 'Item price is required'
            ]
        ]));

        $this->validation->add('item_price', new Numericality([
            'message' => 'Item price must be numeric'
        ]));

        $this->validation->add('item_price', new PositiveValue([
            'message' => 'Item price must be positive'
        ]));

        $this->validation->validate($data);

        return $this;
    }


}