<?php

namespace App\Interfaces;

interface CRUDInterface
{
    public function indexAction();

    public function createAction($id = null);

    public function saveAction();

    public function deleteAction($id);
}