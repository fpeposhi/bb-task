<?php

use App\Validator\ItemsValidator;
use App\Interfaces\CRUDInterface;

class ItemsController extends ControllerBase implements CRUDInterface
{
    public function indexAction()
    {
        $this->view->items = Item::find([
            "order" => "id desc"
        ]);
    }

    public function createAction($id = null)
    {
        try {
            $this->view->item = Item::findFirstById($id);
        } catch (Exception $e) {
            $this->backWithErrorTo("Something went wrong! Item not found", "/items");
            return;
        }
    }

    /**
     * Save or Update an Item
     * */
    public function saveAction()
    {

        $this->validatePostRequestAndRedirect();

        $itemValidator = new ItemsValidator();
        $itemValidator->validate($this->request->getPost());

        if (!$itemValidator->isValid()) {
            return $itemValidator->backWithErrors($this);
        }

        if ($this->request->hasPost('id')) {
            $id = $this->request->getPost("id");
            $item = Item::findFirstById($id);
            if (!$item) {
                $item = new Item();
            }
        } else {
            $item = new Item();
        }

        $item->item_name = $this->request->getPost("item_name");
        $item->item_price = $this->request->getPost("item_price");
        $saved = $item->save();

        if (!$saved) {
            $this->backWithErrors($item->getMessages());
            return;
        }

        $this->response->redirect("/items", true);
    }


    public function deleteAction($id)
    {
        $this->view->disable();

        if (!$this->request->isDelete()) {
            return $this->response->setJsonContent([
                'message' => 'You are not allowed to access this route.'
            ])->setStatusCode(403);
        }

        $item = Item::findFirstById($id);
        if (!$item) {
            return $this->response->setJsonContent([
                'message' => 'Item not found.'
            ])->setStatusCode(404);
        }

        if (count($item->orderItems) > 0) {
            return $this->response->setJsonContent([
                'message' => 'You can not delete this item because is part of some orders.'
            ])->setStatusCode(403);
        }

        $item->delete();

        return $this->response->setJsonContent([
            'message' => 'Item is deleted successfully.'
        ]);
    }


}