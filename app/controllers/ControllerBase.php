<?php

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    /**
     * Validates POST request and redirects back
     *
     * @return boolean
     * */
    protected function validatePostRequestAndRedirect()
    {
        if (!$this->request->isPost()) {
            $this->backWithError("Method not allowed");
            return false;
        }
        if (!$this->security->checkToken()) {
            $this->backWithError("Valid CSRF is required");
            return false;
        }
        return true;
    }

    /**
     * Send user back with errors
     * @param $messages mixed
     *
     * @return void
     * */
    protected function backWithErrors($messages)
    {
        if (!is_array($messages)) {
            $messages = [$messages];
        }
        foreach ($messages as $message) {
            $this->flashSession->error($message);
        }

        $this->view->disable();
        $this->response->redirect($this->request->getHTTPReferer(), true);
    }
    /**
     * Send user back with error
     * @param $message string
     *
     * @return void
     * */
    protected function backWithError($message)
    {
        $this->backWithErrors($message);
    }    /**
     * Send user back with error
     * @param $message string
     *
     * @return void
     * */
    protected function backWithErrorTo($message, $route)
    {
        $this->flashSession->error($message);
        $this->response->redirect($route, true);
    }
}
