<?php


use App\Interfaces\CRUDInterface;
use App\Validator\UsersValidator;

class UsersController extends ControllerBase implements CRUDInterface
{

    public function indexAction()
    {
        $this->view->users = User::find([
            'order' => 'id asc'
        ]);
    }

    public function createAction($id = null)
    {

    }

    public function editAction($id)
    {
        try {
            $this->view->user = User::findFirst([
                "conditions" => "id = ?1",
                "bind" => [
                    1 => $id
                ]
            ]);

            if (!$this->view->user) {
                $this->backWithError('User not found');
                return;
            }
        } catch (Exception $e) {
            $this->backWithErrorTo("Something went wrong! User not found", "/users");
            return;
        }
    }

    public function saveAction()
    {
        $this->validatePostRequestAndRedirect();

        $usersValidator = new UsersValidator();
        $usersValidator->validate($this->request->getPost());

        if (!$usersValidator->isValid()) {
            return $usersValidator->backWithErrors($this);
        }

        $request = $this->request;

        if ($request->hasPost("id")) {
            $id = $request->getPost("id");
            $user = User::findFirst([
                "conditions" => "id = ?1",
                "bind" => [
                    1 => $id
                ]
            ]);

            if (!$user) {
                //id does not exists,
                $this->backWithError("User does not exist!");
                return;
            }
        } else {
            //we are not on edit, we are creating new user
            $user = new User();
        }

        $user->name = $request->getPost("name");
        $user->surname = $request->getPost("surname");
        $user->email = $request->getPost("email");
        $user->updated_at = date("Y-m-d H:i:s");

        $saved = $user->save();
        if (!$saved) {
            $this->backWithErrors($user->getMessages());
            return;
        }
        $this->flashSession->success("User saved successfully.");

        $this->response->redirect("/users/index", true);
        $this->view->disable();
    }


    public function deleteAction($id)
    {
        $this->view->disable();

        if (!$this->request->isDelete()) {
            return $this->response->setJsonContent([
                'message' => 'You are not allowed to access this route.'
            ])->setStatusCode(403);
        }

        $user = User::findFirstById($id);
        if (!$user) {
            return $this->response->setJsonContent([
                'message' => 'User not found.'
            ])->setStatusCode(404);
        }

        if (count($user->orders) > 0) {
            return $this->response->setJsonContent([
                'message' => 'You can not delete this user because he/she has orders linked with. Delete them before.'
            ])->setStatusCode(403);
        }
        $user->delete();

        return $this->response->setJsonContent([
            'message' => 'User is deleted successfully.'
        ]);
    }


}