<?php


use App\Interfaces\CRUDInterface;
use App\Validator\OrdersValidator;

class OrdersController extends ControllerBase implements CRUDInterface
{
    public function indexAction()
    {
        $this->view->orders = Order::find([
            'order' => 'created_at desc'
        ]);
    }

    public function userAction($id)
    {
        try{
        $this->view->user = User::findFirst([
            'conditions' => 'id = ?1',
            'bind' => [
                1 => $id
            ]
        ]);
        }catch (Exception $e){
            $this->backWithErrorTo("Something went wrong! Orders of user not found", "/users");
            return;
        }
    }

    public function createAction($id = null)
    {
        $this->view->users = User::find();
        $this->view->items = Item::find();
    }

    public function editAction($id)
    {
        try {
            $this->view->order = Order::findFirst([
                'conditions' => 'id = ?1',
                'bind' => [
                    1 => $id
                ]
            ]);

            if (!$this->view->order) {
                $this->backWithError('Order not found');
                return;
            }

            $this->view->users = User::find();
            $this->view->items = Item::find();
        }catch (Exception $e){
            $this->backWithErrorTo("Something went wrong! Order not found", "/orders");
            return;
        }
    }

    public function saveAction()
    {
        $this->validatePostRequestAndRedirect();

        $orderValidation = new OrdersValidator();
        $orderValidation->validate($this->request->getPost());

        if (!$orderValidation->isValid()) {
            $orderValidation->backWithErrors($this);
            return;
        }

        $user = User::findFirstById($this->request->getPost('id_user'));
        if (!$user) {
            $this->backWithError("Selected user does not exist!");
            return;
        }

        //get order object from db if the id is provided. Case: User is in Edit
        if($this->request->hasPost('id')){
            $id = $this->request->getPost('id');
            $order = Order::findFirstById($id);
            if(!$order){
                $order = new Order();
            }
        }else{
            //create new order object
            $order = new Order();
        }

        $order->user = $user;
        $order->order_date = $this->request->getPost("order_date");
        $order->order_no = $this->request->getPost("order_no");
        $order->order_notes = $this->request->getPost("order_notes");
        $order->order_amount = 0;

        $orderItemsObjs = [];

        $orderItems = $this->request->getPost('item_id');
        $quantities = $this->request->getPost('quantity');

        if(count($orderItems) == 0 || count($quantities) == ''){
            $this->backWithError("You must select at least one item for this order!");
            return;
        }

        foreach ($orderItems as $key => $itemId) {
            $quantity = $quantities[$key];
            if(!is_numeric($itemId) ){
                $this->backWithError("Item selected is not valid!");
                return;
            }
            if(!is_numeric($quantity) || $quantity <= 0 ){
                $this->backWithError("Item Quantity is required!");
                return;
            }

            if(!is_numeric($quantity) || $quantity <= 0){
                $this->backWithError("Quantity for Item is not valid!");
                return;
            }

            $itemObj = Item::findFirstById($itemId);

            if (!$itemObj) {
                $this->backWithError("Item selected is not valid!");
                continue;
            }

            $orderItem = new OrderItems();
            $orderItem->item = $itemObj;
            $orderItem->quantity = $quantities[$key];

            $order->order_amount += $itemObj->item_price * $orderItem->quantity;
            $orderItemsObjs[] = $orderItem;
        }

        if($order->id){
            $order->orderItems->delete();
        }

        $order->orderItems = $orderItemsObjs;

        $saved = $order->save();
        if (!$saved) {
            $this->backWithErrors($order->getMessages());
            return;
        }

        $this->response->redirect("/orders", true);
    }

    public function deleteAction($id){
        $this->view->disable();
        if(!$this->request->isDelete()){
            return $this->response->setJsonContent([
                'message' => 'You are not allowed to access this route.'
            ])->setStatusCode(403);
        }

        $order = Order::findFirstById($id);
        if(!$order){
            return $this->response->setJsonContent([
                'message' => 'Order not found.'
            ])->setStatusCode(404);
        }

        $order->orderItems->delete();
        $order->delete();

        return $this->response->setJsonContent([
            'message' => 'Order is deleted successfully.'
        ]);
    }

}