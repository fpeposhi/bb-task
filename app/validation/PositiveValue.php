<?php

use Phalcon\Validation\Message;

class PositiveValue extends \Phalcon\Validation\Validator
{

    /**
     * Executes the validation
     *
     * @param \Phalcon\Validation $validation
     * @param string $attribute
     * @return bool
     */
    public function validate(\Phalcon\Validation $validation, $attribute)
    {
        $value = $validation->getValue($attribute);

        $message = $this->getOption('message');
        if (!$message) {
            //message was not provided, so set some default
            $message = "This number is not positive!";
        }

        if (!is_numeric($value)){
            $validation->appendMessage(new Message($message, $attribute));
            return false;
        }
        if ($value < 0){
            $validation->appendMessage(new Message($message, $attribute));
            return false;
        }


        return true;
    }

    /**
     * Get the template message
     *
     * @throw InvalidArgumentException When the field does not exists
     * @param string $field
     * @return string
     */
    public function getTemplate(string $field): string
    {
        // TODO: Implement getTemplate() method.
    }

    /**
     * Get message templates
     *
     * @return array
     */
    public function getTemplates(): array
    {
        // TODO: Implement getTemplates() method.
    }

    /**
     * Clear current template and set new from an array,
     *
     * @param array $templates
     * @return \Phalcon\Validation\ValidatorInterface
     */
    public function setTemplates(array $templates): \Phalcon\Validation\ValidatorInterface
    {
        // TODO: Implement setTemplates() method.
    }

    /**
     * Set a new temlate message
     *
     * @param string $template
     * @return \Phalcon\Validation\ValidatorInterface
     */
    public function setTemplate(string $template): \Phalcon\Validation\ValidatorInterface
    {
        // TODO: Implement setTemplate() method.
    }
}