if ($('.item-container').length) {
    $(function () {
        $('.btn-delete-item').click(function () {
            var id = $(this).data('id');

            var status = confirm("Are you sure You want to delete this item?")
            if (!status) {
                return;
            }
            $.post({
                "url": "/items/delete/" + id,
                "type": 'delete',
                success: function (response) {
                    alert(response.message)
                    window.location.href = "/items"
                },
                error: function (response) {
                    alert(response.responseJSON.message)
                }
            })
        })
    })
}