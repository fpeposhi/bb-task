if($('table.table-order-items').length) {
    var rowItem = null;
    var parent = $('table.table-order-items');
    $(function () {
        rowItem = $('table.table-order-items tbody tr:first').clone();

        $('.btn-add-order-item').click(function () {
            var _row = rowItem.clone();
            $('table.table-order-items tbody').append($('<tr/>').html(_row.html()));
            $('table.table-order-items tbody tr:last-child select').val('');
            $('table.table-order-items tbody tr:last-child input').val('1');
            $('table.table-order-items tbody tr:last-child .price').text('');
            initListeners().trigger('change')
        })

        initListeners().change();

        $('.btn-delete-order').click(function () {
            var id = $(this).data('id');

            var status = confirm("Are you sure You want to delete this order ?")
            if(!status){
                return;
            }
            $.post({
                "url" : "/orders/delete/"+id,
                "type" : 'delete',
                success : function (response) {
                    alert(response.message)
                    window.location.href = "/orders"
                },
                error: function (response) {
                    alert(response.responseJSON.message)
                }
            })
        })
    })

    var initListeners = function(){

        parent.find('button.btn-remove-item').off('click').click(function () {
            $(this).closest('tr').remove();
            calculateTotal()
        })

        parent.find('input.item-quantity').off('change').change(function () {
            calculateTotal()
        })

        return parent.find('select.item').off('change').change(function () {
            var row = $(this).closest('tr');
            row.find('span.price').text($(this).find('option:selected').attr('data-price'));

            calculateTotal()
        })

    }

    var calculateTotal = function () {
        var total = 0;
        parent.find('tbody tr').each(function () {
            var price = $(this).find('select.item').find('option:selected').attr('data-price');
            if( typeof price == 'undefined'){
                return true
            }
            total += $(this).find('input.item-quantity').val() * price;
            console.log(price)
        });



        $('span.total').text(total.toFixed(2))
    }
}