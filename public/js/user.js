if ($('.user-container').length) {
    $(function () {
        $('.btn-delete-user').click(function () {
            var id = $(this).data('id');

            var status = confirm("Are you sure You want to delete this User?")
            if (!status) {
                return;
            }
            $.post({
                "url": "/users/delete/" + id,
                "type": 'delete',
                success: function (response) {
                    alert(response.message)
                    window.location.href = "/users"
                },
                error: function (response) {
                    alert(response.responseJSON.message)
                }
            })
        })
    })
}