
    <div class="row mt-2 item-container">
        <div class="col-md-12">
            <div class="page-header">
                <?php if ($item) { ?>
                    <h3>Edit Item <?= $item->item_name ?>
                        <a href="/items" class="btn btn-info float-right">Back to list</a>
                    </h3>
                <?php } else { ?>
                    <h3>Add Item
                        <a href="/items" class="btn btn-info float-right">Back to list</a>
                    </h3>
                <?php } ?>
            </div>

        </div>
    </div>

    <div class="row mt-2">
        <div class="col-md-12">
            <?php foreach ($this->flashSession->getMessages('error') as $message) { ?>
                <div class="alert alert-danger">
                    <?= $message ?>
                </div>
            <?php } ?>

            <form action="/items/save" class="form" method="post">

                <?php if ($item) { ?>
                    <input type="hidden" name="id" value="<?= $item->id ?>">
                <?php } ?>

                <input type='hidden'
                       name='<?= $this->security->getTokenKey() ?>'
                       value='<?= $this->security->getToken() ?>'/>

                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="name">Name  <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" <?php if ($item) { ?> value="<?= $item->item_name ?>" <?php } ?> name="item_name" >
                    </div>

                    <div class="form-group col-md-3">
                        <label for="price">Price  <span class="text-danger">*</span></label>
                        <input type="number" class="form-control" <?php if ($item) { ?> value="<?= $item->item_price ?>" <?php } ?>  name="item_price"  >
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-7 ">
                        <hr>
                        <?php if ($item) { ?>
                        <button class="btn btn-danger float-left btn-delete-item" data-id="<?= $item->id ?>" type="button">Delete Item</button>
                        <?php } ?>
                        <a class=" float-right btn btn-link" href="/items">Cancel</a>
                        <button class="btn btn-success float-right" type="submit">Save Details</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
