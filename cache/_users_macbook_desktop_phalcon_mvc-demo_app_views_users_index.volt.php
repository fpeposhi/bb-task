<div class="row mt-2">
    <div class="col-md-12">
        <div class="page-header">
            <h3>Users
                <a href="/users/create" class="btn btn-info float-right">Add new user</a>
            </h3>
        </div>

    </div>
</div>


<div class="row col-md-12 mt-2">

    <?php foreach ($this->flashSession->getMessages('warning') as $message) { ?>
        <div class="alert alert-warning">
            <?= $message ?>
        </div>
    <?php } ?>

    <?php foreach ($this->flashSession->getMessages('error') as $message) { ?>
        <div class="alert alert-danger">
            <?= $message ?>
        </div>
    <?php } ?>

    <table class="table table-stripped">
        <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th></th>
            <th></th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($users as $user) { ?>
            <tr>
                <td><?= $user->name ?> <?= $user->surname ?></td>
                <td><?= $user->email ?></td>
                <td>
                    <a href="/users/edit/<?= $user->id ?>" class="">Edit</a>
                </td>
                <td>
                    <a href="/orders/user/<?= $user->id ?>" class="">Orders</a>
                </td>
            </tr>
        <?php } ?>

        </tbody>

    </table>

</div>
