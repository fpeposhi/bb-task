

<div class="page-header">
    <h1> Users Registered </h1>
</div>



<table class="table table-stripped">
    <thead>
    <tr>
        <th>Name</th>
        <th>Email</th>
        <th></th>
    </tr>
    </thead>

    <tbody>
    <?php foreach ($users as $user) { ?>
        <tr>
            <td><?= $user->name ?> <?= $user->surname ?></td>
            <td><?= $user->email ?></td>
            <td><a href="/auth/edit/<?= $user->id ?>"  class="">Edit</a></td>
        </tr>
    <?php } ?>

    </tbody>

</table>

<a href="/auth/register" class="btn btn-info">Add new user</a>