
<div class="row mt-2 user-container">
    <div class="col-md-12">
        <div class="page-header">
            <h3>Edit <?= $user->name ?> <?= $user->surname ?>
                <a href="/users" class="btn btn-info float-right">Back to list</a>
            </h3>
        </div>

    </div>
</div>

<div class="row mt-2">
    <div class="col-md-12">

        <?php foreach ($this->flashSession->getMessages('error') as $message) { ?>
            <div class="alert alert-danger">
                <?= $message ?>
            </div>
        <?php } ?>

        <form action="/users/save" class="form" method="post">

            <input type='hidden'
                   name='<?= $this->security->getTokenKey() ?>'
                   value='<?= $this->security->getToken() ?>'/>

            <input type="hidden" name="id" value="<?= $user->id ?>">
            <div class="row">
                <div class="form-group col-md-3">
                    <label for="name">Name <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="name" value="<?= $user->name ?>">
                </div>

                <div class="form-group col-md-3">
                    <label for="name">Surname <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="surname" value="<?= $user->surname ?>">
                </div>

            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="name">Email <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="email" value="<?= $user->email ?>">
                </div>

            </div>
            <div class="row">
                <div class="col-md-6 ">
                    <hr>
                    <button class="btn btn-danger float-left btn-delete-user" data-id="<?= $user->id ?>" type="button">Delete User</button>

                    <a class=" float-right btn btn-link" href="/users">Cancel</a>
                    <button class="btn btn-success float-right" type="submit">Save Details</button>
                </div>
            </div>

        </form>
    </div>
</div>
