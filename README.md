MVC Demo application

Design a model where we have entities:

●     User, Order and Order items (with appropriate relations defined)

●     Prepare a simple form which will save data of an Order to a database (please also use some form validations)

●     Create a page which will list all the Orders

Please use PHP 7.*, Phalcon 3.*, PostgreSQL 11+

Usage of ORM layer is highly encouraged.